package models;
import java.util.ArrayList;

public interface dbPersistencia {
        public void insertar(Object objecto)throws Exception;
        public void actualizar(Object objecto)throws Exception;
        public void habilitar(Object object, String codigo)throws Exception;
        public void deshablitar(Object object, String codigo)throws Exception;
        
        public boolean isExiste(String codigo)throws Exception;
        public ArrayList lista()throws Exception;
        public ArrayList lista(String criterio)throws Exception;
        public ArrayList lista2()throws Exception;
        
        public Object buscar(int id) throws Exception;
        public Object buscardes(String codigo)throws Exception;
        public Object buscar(String codigo)throws Exception;
}
